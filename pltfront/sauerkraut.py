import base64
import secrets
from cryptography.fernet import Fernet, InvalidToken
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

KDF_ALGORITHM = hashes.SHA256()
KDF_LENGTH = 32
KDF_ITERATIONS = 120000

def encrypt(plain, password):
    """
    Derive a symmetric key using the passsword and a fresh random salt to encrypt a plaintext input.

    Parameters
    ----------
    plain:      str or bytes
                plain text string to be encrypted.
    password:   str
                password used to derive the symmetric key.
    """
    
    salt = secrets.token_bytes(16)
    kdf = PBKDF2HMAC(
        algorithm=KDF_ALGORITHM, length=KDF_LENGTH, salt=salt,
        iterations=KDF_ITERATIONS)
    key = kdf.derive(password.encode("utf-8"))

    # Encrypt the message.
    f = Fernet(base64.urlsafe_b64encode(key))
    cipher = f.encrypt(plain)

    return cipher, salt

def decrypt(cipher, password, salt):
    """
    Derive the symmetric key using the password and provided salt to decrypt a ciphertext input.

    Parameters
    ----------
    cipher:     str or bytes
                plain text string to be decrypted.
    password:   str
                password used to derive the symmetric key.
    """

    kdf = PBKDF2HMAC(
        algorithm=KDF_ALGORITHM, length=KDF_LENGTH, salt=salt,
        iterations=KDF_ITERATIONS)
    key = kdf.derive(password.encode("utf-8"))

    # Decrypt the message
    f = Fernet(base64.urlsafe_b64encode(key))
    try:
        plain = f.decrypt(cipher)
    except InvalidToken:
        raise RuntimeError('Wrong password for cipher decoding')

    return plain

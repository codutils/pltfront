pltfront
========

**pltfront** is a bare-bones Python plotting package, which provides an easy interface for `matplotlib` and removes some complexity from repetitive plotting tasks.

These pages aim to showcase some basic features of **pltfront**.

.. toctree::
    :maxdepth: 2
    :caption: Features
    
    basics
    save
    store
    


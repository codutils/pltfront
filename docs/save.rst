Updating and showing plots
==========================

Plots generated using ``Plot.plot`` can be modified, saved and shown
even after being drawn.

Update a plot
-------------

We want to modify an already existing plot, after it has been generated
and shown. By default, ``plot`` returns ``fig, ax`` objects and saves
them “behind the scenes” as class attributes,
``plotting.last_fig, plotting.last_ax``. We can modify the last drawn
plot by using the ``update`` method, which allows us to change axes and
figure properties. Furthermore, ``update`` behaves like ``plot``:
``fig, ax`` objects are returned and can be saved or stored as usual.

.. code:: ipython3

    import os
    from pltfront.plot import Plot
    import numpy as np
    
    # Output directory
    out = os.path.join(os.getcwd(), 'out')
    # Generate the data set
    xs = [np.linspace(0., 1.25), np.linspace(0., 1.25)]
    ys = [np.exp(xs[0]), np.exp(2*xs[1])]
    kinds = ['line', 'scatter']
    labels = ['$e^x$', '$e^{2x}$']
    
    # Plot
    plotting = Plot(out=out)
    fig, _ = plotting.plot(xs, ys, kind=kinds, size=[5, 2.5], label=labels, legend=True, show=True, close=True)
    # Change plot scale to loglog
    _ = plotting.update(xlabel='x axis', ylabel='y axis', xscale='log', yscale='log', show=True, close=True)



.. image:: save_1.png



.. image:: save_2.png


Show a plot
-----------

We can show the any plot we generated using the ``show`` method. Like
with ``update``, the last generated plot is automatically shown if no
figure is specified.

.. code:: ipython3

    plotting.show()



.. image:: save_3.png


Save a plot
-----------

We can save any updated figure *after the fact* using the ``save``
method.

.. code:: ipython3

    plotting.save('updated', 'tight', fig=fig)



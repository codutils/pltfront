To do list
==========
Items in **bold** are particularly significant and should be implemented first.

`Plot`
------
#### Major
- [ ] Add `subplots` method, which calls `plot` to fill up specified subplot structure
- [ ] Refactor `movie` class using `matplotlib.animation`: add `animate` method which loads stored figures and axes to produce a movie
- [x] Secure unpickler to avoid malicious unpickling
- [X] Add `fig, ax` object pickling for future use
#### Minor
- [X] Add automatic x coordinate filling is a single value is provided
- [X] Add `update` method to `Plot` to replot last `plot` with new settings
#### Patches
- [ ] Add plot variables as class variables
- [X] Fix kind setting when none is specified - it should always be line
- [X] Preserve `fig, ax` as class attributes after `Plot.plot` is called
- [X] Add label handling to `plot_kwargs`: if user provides label list, it's passed to the appropriate `plot_kwargs` dict entry, item by item
- [X] Employ `ax.set` method instead of `xlim`, `ylim`, ...
  or scatter (now it's mixed: it first sets line, then scatter for subsequent
  missing kinds)
- [X] Silence label warning when legend is False

`PlotFactory`
-------------
- [ ] Add 3D plotting support, perhaps automatically
- [ ] Add histogram support, maintaining all stylistic choices made for `plot`
- [ ] Add `axhline`, `axvline` using `*list` syntax for `q_min, q_max`
- [X] Deprecate `semilogx`, `semilogy`, `loglog` in favor of `xscale`, `yscale`

API
---
- [ ] Add API for CLI usage (e.g. with data files)
- [ ] Add gnuplot syntax recognition in API (perhaps with object pickling on matplotlib side)
